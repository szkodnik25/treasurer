﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Skarbnik
{
    public sealed partial class EditPersonDialog : ContentDialog
    {
        private float obligation;
        public float payment { get; private set; }
        public bool isDeleteClicked { get; private set; }

        public EditPersonDialog(float obligation)
        {
            this.isDeleteClicked = false;
            this.payment = obligation;
            this.InitializeComponent();
            this.paymentTextBox.Text = payment.ToString();
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            payment = float.Parse(paymentTextBox.Text);
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }

        private void resetButton_Click(object sender, RoutedEventArgs e)
        {
            this.payment = 0;
            paymentTextBox.Text = payment.ToString();
        }

        private void plusTenButton_Click(object sender, RoutedEventArgs e)
        {
            this.payment = float.Parse(paymentTextBox.Text) + 10;
            paymentTextBox.Text = payment.ToString();
        }

        private void plusTwentyButton_Click(object sender, RoutedEventArgs e)
        {
            this.payment = float.Parse(paymentTextBox.Text) + 20;
            paymentTextBox.Text = payment.ToString();
        }

        private void plusFiftyButton_Click(object sender, RoutedEventArgs e)
        {
            this.payment = float.Parse(paymentTextBox.Text) + 50;
            paymentTextBox.Text = payment.ToString();
        }

        private void plusHundredButton_Click(object sender, RoutedEventArgs e)
        {
            this.payment = float.Parse(paymentTextBox.Text) + 100;
            paymentTextBox.Text = payment.ToString();
        }

        private void plusTwoHundredButton_Click(object sender, RoutedEventArgs e)
        {
            this.payment = float.Parse(paymentTextBox.Text) + 200;
            paymentTextBox.Text = payment.ToString();
        }

        private void deletePersonButton_Click(object sender, RoutedEventArgs e)
        {
            this.isDeleteClicked = true;
            this.Hide();
        }
    }
}
