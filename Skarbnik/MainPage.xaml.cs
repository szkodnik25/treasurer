﻿using Skarbnik.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Skarbnik
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public ObservableCollection<FundRaiserData> observableFundRaiserData = new ObservableCollection<FundRaiserData>();
        public List<FundRaiserData> fundRaisers = new List<FundRaiserData>();
        private JsonSerializer<List<FundRaiserData>> mySerializer;

        public MainPage()
        {
            mySerializer = new JsonSerializer<List<FundRaiserData>>();
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            RefreshDataAndShowProgressIndicator("Wczytuję dane");
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: Prepare page for display here.

            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void AddFundRaiserButton_Click(object sender, RoutedEventArgs e)
        {
            showNewFundRaiserDialogAndGetInput();
        }

        private async void forceSaveDataButton_Click(object sender, RoutedEventArgs e)
        {
            await mySerializer.WriteDataToJsonFileAsync(fundRaisers);
        }

        private void AboutAppButton_Click(object sender, RoutedEventArgs e)
        {
            showAboutAppDialog();
        }

        private async void showNewFundRaiserDialogAndGetInput()
        {
            NewFundRaiserDialog dialog = new NewFundRaiserDialog();
            ContentDialogResult result = await dialog.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                if (dialog.data != null)
                {
                    observableFundRaiserData.Add(dialog.data);
                    fundRaisers.Add(dialog.data);
                    FundRaiserGeneralListView.DataContext = observableFundRaiserData;
                }
            }
            await mySerializer.WriteDataToJsonFileAsync(fundRaisers);
            RefreshDataAndShowProgressIndicator("Zapisuje dane");
        }

        private async void showAboutAppDialog()
        {
            string title =      "O aplikacji";
            string message =    "Aplikacja stworzona na potrzeby zaliczenia przedmiotu Programowanie Aplikacji Windows Phone 8/8.1." + Environment.NewLine +
                                "Autor: Mateusz Wrzos.";

            var messageDialog = new MessageDialog(message, title);
            await messageDialog.ShowAsync();
        }

        private async void RefreshDataButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshDataAndShowProgressIndicator("Odświeżam");
        }

        private async void RefreshDataAndShowProgressIndicator(string indicatorText)
        {
            StatusBarProgressIndicator indicator = StatusBar.GetForCurrentView().ProgressIndicator;
            indicator.Text = indicatorText;
            indicator.ShowAsync();

            try
            {
                var file = await ApplicationData.Current.LocalFolder.GetFileAsync("data.json");
            }
            catch
            {
                indicator.HideAsync();
                return;
            }

            refreshAndReplaceListView(await mySerializer.DeserializeJsonFile());
            System.Diagnostics.Debug.WriteLine(await mySerializer.ReadDataFromJsonFileToString());
            indicator.HideAsync();
        }

        private void refreshAndReplaceListView(List<FundRaiserData> list)
        {
            observableFundRaiserData = new ObservableCollection<FundRaiserData>();
            fundRaisers = new List<FundRaiserData>();

            foreach (FundRaiserData fundRaiserData in list)
            {
                observableFundRaiserData.Add(fundRaiserData);
                fundRaisers.Add(fundRaiserData);
            }

            FundRaiserGeneralListView.DataContext = observableFundRaiserData;
        }

        private void FundRaiserGeneralListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clicked = ((FundRaiserData)e.ClickedItem).fundRaiserName;
            Frame.Navigate(typeof(FundRaiserPage), clicked);
        }

    }
}
