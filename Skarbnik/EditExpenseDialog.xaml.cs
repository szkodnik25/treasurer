﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Skarbnik
{
    public sealed partial class EditExpenseDialog : ContentDialog
    {
        public Expense expense { get; private set; }
        public bool deleteButtonClicked { get; private set; }

        public EditExpenseDialog(Expense expense)
        {
            this.deleteButtonClicked = false;
            this.InitializeComponent();
            this.expense = expense;
            this.InitializeViewComponentsWithExpenseData();
        }

        private void InitializeViewComponentsWithExpenseData()
        {
            this.expenseNameTextBox.Text = expense.expenseName;
            this.pricePerOneTextBox.Text = expense.pricePerOne.ToString();
            this.countTextBox.Text = expense.count.ToString();
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            string expenseName = expenseNameTextBox.Text;
            float pricePerOne = float.Parse(pricePerOneTextBox.Text);
            UInt32 count = UInt32.Parse(countTextBox.Text);

            if (expenseName == null || pricePerOne == null || count == null)
            {
                return;
            }

            this.expense = new Expense(expenseName, pricePerOne, count);
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }

        private void deleteExpenseButton_Click(object sender, RoutedEventArgs e)
        {
            this.deleteButtonClicked = true;
            this.Hide();
        }
    }
}
