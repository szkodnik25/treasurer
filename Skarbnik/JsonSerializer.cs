﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Windows.Storage;

namespace Skarbnik
{
    class JsonSerializer<Type>
    {
        public const string FILENAME = "data.json";

        public JsonSerializer() { }

        public async Task WriteDataToJsonFileAsync(Type data)
        {
            var serializer = new DataContractJsonSerializer(typeof(Type));

            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(FILENAME, CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, data);
            }
        }

        public async Task<String> ReadDataFromJsonFileToString(string filename = FILENAME)
        {
            String content = String.Empty;
            var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(filename);
            using (StreamReader reader = new StreamReader(stream))
            {
                content = await reader.ReadToEndAsync();
            }
            return content;
        }

        public async Task<Type> deserializeJsonFile(string filemane = FILENAME)
        {
            var jsonSerializer = new DataContractJsonSerializer(typeof(Type));
            var stream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(filemane);
            return (Type) jsonSerializer.ReadObject(stream); ;
        }
    }
}
