﻿using Skarbnik.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Skarbnik
{
    public sealed partial class NewFundRaiserDialog : ContentDialog
    {
        public FundRaiserData data { get; private set; }

        public NewFundRaiserDialog()
        {
            data = null;
            this.InitializeComponent();
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            string name = fundRaiserName.Text;
            string description = fundRaiserDescription.Text;

            if (name.Length != 0)
            {
                if (description.Length == 0) description = "<Brak opisu>";
                this.data = new FundRaiserData(name, description);
            }
            else if (description.Length != 0)
            {
                return;
            }

            
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }
    }
}
