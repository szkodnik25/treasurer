﻿

#pragma checksum "C:\Users\mtwrz_000\SkyDrive\Workspace\WP8\Skarbnik\Skarbnik\EditPersonDialog.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F25C2AF804738126D1764EFCD32E91CD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Skarbnik
{
    partial class EditPersonDialog : global::Windows.UI.Xaml.Controls.ContentDialog
    {
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.TextBox paymentTextBox; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button deletePersonButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button plusHundredButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button plusTwoHundredButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button resetButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button plusTenButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button plusTwentyButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private global::Windows.UI.Xaml.Controls.Button plusFiftyButton; 
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        private bool _contentLoaded;

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 4.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent()
        {
            if (_contentLoaded)
                return;

            _contentLoaded = true;
            global::Windows.UI.Xaml.Application.LoadComponent(this, new global::System.Uri("ms-appx:///EditPersonDialog.xaml"), global::Windows.UI.Xaml.Controls.Primitives.ComponentResourceLocation.Application);
 
            paymentTextBox = (global::Windows.UI.Xaml.Controls.TextBox)this.FindName("paymentTextBox");
            deletePersonButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("deletePersonButton");
            plusHundredButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("plusHundredButton");
            plusTwoHundredButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("plusTwoHundredButton");
            resetButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("resetButton");
            plusTenButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("plusTenButton");
            plusTwentyButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("plusTwentyButton");
            plusFiftyButton = (global::Windows.UI.Xaml.Controls.Button)this.FindName("plusFiftyButton");
        }
    }
}



