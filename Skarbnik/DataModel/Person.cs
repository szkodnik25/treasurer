﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Skarbnik.DataModel
{
    [DataContract]
    public class Person
    {
        [DataMember]
        public string name { get; private set; }
        [DataMember]
        public string email { get; private set; }
        [DataMember]
        public float obligation { get; set; }

        public Person(string name, string email, float obligation = 0)
        {
            this.name = name;
            this.email = email;
            this.obligation = obligation;
        }

        public override string ToString()
        {
            return String.Format("{0} ({1}): {2} PLN", name, email, obligation);
        }

    }
}
