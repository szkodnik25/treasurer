﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Skarbnik
{
    [DataContract]
    public class Expense
    {
        [DataMember]
        public string expenseName { get; private set; }
        [DataMember]
        public float pricePerOne { get; private set; }
        [DataMember]
        public UInt32 count { get; private set; }

        public Expense(string expenseName, float price, UInt32 count = 1)
        {
            this.expenseName = expenseName;
            this.pricePerOne = price;
            this.count = count;
        }

        public override string ToString()
        {
            return String.Format("{0}: {1} x {2} PLN", expenseName, count, pricePerOne);
        }

        public float SummaryExpenseCost()
        {
            return this.pricePerOne * this.count;
        }
    }
}
