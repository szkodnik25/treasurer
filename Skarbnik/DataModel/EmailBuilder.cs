﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Email;

namespace Skarbnik.DataModel
{
    class EmailBuilder
    {
        public FundRaiserData fundRaiser { get; private set; }
        private EmailMessage emailMessage;

        public EmailBuilder(FundRaiserData fundRaiser)
        {
            this.fundRaiser = fundRaiser;
        }

        public EmailMessage BuildEmailMessage()
        {
            emailMessage = new EmailMessage();
            emailMessage.Subject = fundRaiser.fundRaiserName;
            BuildRecipientsList();
            emailMessage.Body = BuildHeaderMessage() + BuildExpensesMessage() + BuildParticipantsMessage();
            return emailMessage;
        }

        private void BuildRecipientsList()
        {
            foreach (Person person in fundRaiser.participants)
            {
                emailMessage.To.Add(new EmailRecipient(person.email, person.name));
            }
        }

        private string BuildHeaderMessage()
        {
            return String.Format("Wiadomość w związku ze zbiórką: {0}: {1}." + Environment.NewLine, fundRaiser.fundRaiserName, fundRaiser.fundRaiserDescription);
        }

        private string BuildExpensesMessage()
        {
            string expensesList = "Lista wydatków: " + Environment.NewLine;
            foreach (Expense expense in fundRaiser.expenses)
            {
                expensesList += expense.ToString() + Environment.NewLine;
            }
            return expensesList;
        }

        private string BuildParticipantsMessage()
        {
            string participants = "Uczestnicy:" + Environment.NewLine;

            foreach (Person person in fundRaiser.participants)
            {
                participants += person.ToString() + Environment.NewLine;
            }

            return participants;
        }
    }
}
