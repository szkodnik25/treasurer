﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Skarbnik.DataModel
{
    [DataContract]
    public class FundRaiserData
    {
        [DataMember]
        public string fundRaiserName { get; private set; }
        [DataMember]
        public string fundRaiserDescription { get; private set; }
        [DataMember]
        public List<Expense> expenses { get; set; }
        [DataMember]
        public List<Person> participants { get; set; }

        public FundRaiserData(string fundRaiserName, string fundRaiserDescription)
        {
            this.fundRaiserName = fundRaiserName;
            this.fundRaiserDescription = fundRaiserDescription;
            this.participants = new List<Person>();
            this.expenses = new List<Expense>();
        }

        public void AddNewExpense(Expense expense)
        {
            this.expenses.Add(expense);
        }

        public bool ExpenseExist(Expense newExpense)
        {
            foreach (Expense expense in this.expenses)
            {
                if (expense.expenseName.Equals(newExpense.expenseName))
                {
                    return true;
                }
            }

            return false;
        }

        public bool ParticipantExist(Person newPerson)
        {
            foreach (Person person in this.participants)
            {
                if (person.email.Equals(newPerson.email) && person.name.Equals(newPerson.name))
                {
                    return true;
                }
            }

            return false;
        }

        public void AddNewParticipant(Person newParticipant)
        {
            this.participants.Add(newParticipant);
        }

        public float SumUpExpences()
        {
            float result = 0;
            foreach (Expense expense in this.expenses)
            {
                result += expense.count * expense.pricePerOne; 
            }
            return result;
        }

        public float CalculateObligationPerOne()
        {
            int numberOfParticipants = participants.Count;
            float summaryObligation = this.SumUpExpences();
            return (summaryObligation / numberOfParticipants);
        }
    }
}
