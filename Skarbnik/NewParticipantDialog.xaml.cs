﻿using Skarbnik.DataModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Skarbnik
{
    public sealed partial class NewParticipantDialog : ContentDialog
    {
        public Person person { get; private set; }
        public bool pickFromContacts { get; private set; }

        public NewParticipantDialog()
        {
            person = null;
            pickFromContacts = false;
            this.InitializeComponent();
        }

        private void ContentDialog_PrimaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
            string name = Name.Text;
            string email = Email.Text;

            if (name.Length != 0 && email.Length != 0)
            {
                this.person = new Person(name, email);
            }
            else
            {
                return;
            }
        }

        private void ContentDialog_SecondaryButtonClick(ContentDialog sender, ContentDialogButtonClickEventArgs args)
        {
        }

        private void ShowContactsButton_Click(object sender, RoutedEventArgs e)
        {
            this.pickFromContacts = true;
            this.Hide();
        }
    }
}
