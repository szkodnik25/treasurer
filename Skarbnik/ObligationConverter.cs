﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace Skarbnik
{
    public class ObligationConverter : IValueConverter
    {
        private readonly Color positiveColor = Colors.Green;
        private readonly Color negativeColor = Colors.Red;
        private readonly Color defaultColor = Colors.White;

        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var obligation = (float)value;

            if (obligation > 0)
            {
                return new SolidColorBrush(positiveColor);
            }
            else if (obligation < 0)
            {
                return new SolidColorBrush(negativeColor);
            }

            return new SolidColorBrush(defaultColor);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }
}
