﻿using Skarbnik.DataModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.ApplicationModel.Contacts;
using Windows.ApplicationModel.Email;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Skarbnik
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FundRaiserPage : Page
    {
        private JsonSerializer<List<FundRaiserData>> mySerializer;
        private List<FundRaiserData> allFundRaisers;
        string passedName;

        private FundRaiserData fundRaiser;

        private ObservableCollection<Person> observablePersons = new ObservableCollection<Person>();
        private ObservableCollection<Expense> observableExpenses = new ObservableCollection<Expense>();

        public FundRaiserPage()
        {
            this.InitializeComponent();
            InitializeSerializerAndFundRaiser();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            passedName = e.Parameter as string;
            UpdateTitle(passedName, 0);
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
            e.Handled = true;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= HardwareButtons_BackPressed;
            base.OnNavigatedFrom(e);
        }

        private void UpdateTitle(string title, float summaryAmount)
        {
            FundRaisersPivot.Title = String.Format("ZBIÓRKA {0} ({1} PLN)", title, summaryAmount);
        }

        private async void InitializeSerializerAndFundRaiser()
        {
            mySerializer = new JsonSerializer<List<FundRaiserData>>();
            allFundRaisers = await mySerializer.DeserializeJsonFile();

            foreach (FundRaiserData element in allFundRaisers)
            {
                if (element.fundRaiserName.Equals(passedName))
                {
                    fundRaiser = element;
                    break;
                }
            }
            RefreshListsAndUpdateTitle();
        }

        private async void ShowDescriptionButton_Click(object sender, RoutedEventArgs e)
        {
            string message = fundRaiser.fundRaiserDescription;
            string title = fundRaiser.fundRaiserName;
            MessageDialog messageDialog = new MessageDialog(message, title);
            await messageDialog.ShowAsync();
        }

        private async void AddParticipantButton_Click(object sender, RoutedEventArgs e)
        {
            ShowNewParticipantDialog();
            System.Diagnostics.Debug.WriteLine(await mySerializer.ReadDataFromJsonFileToString());
        }

        private async void AddExpenseButton_Click(object sender, RoutedEventArgs e)
        {
            NewExpenseDialog newExpenseDialog = new NewExpenseDialog();
            ContentDialogResult result = await newExpenseDialog.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                
                if (fundRaiser.ExpenseExist(newExpenseDialog.newExpense) && fundRaiser.expenses != null)
                {
                    new MessageDialog("Wydatek o tej nazwie już istnieje.", "Błąd").ShowAsync();
                    return;
                }
                
                fundRaiser.AddNewExpense(newExpenseDialog.newExpense);
                observableExpenses.Add(newExpenseDialog.newExpense);
                int index = allFundRaisers.IndexOf(fundRaiser);
                allFundRaisers[index] = fundRaiser;
                await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                ExpensesListView.DataContext = observableExpenses;

                if (fundRaiser.participants != null)
                {
                    foreach (Person person in fundRaiser.participants)
                    {
                        person.obligation -= newExpenseDialog.newExpense.SummaryExpenseCost() / fundRaiser.participants.Count();
                    }
                }
                
                await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                System.Diagnostics.Debug.WriteLine(await mySerializer.ReadDataFromJsonFileToString());
                RefreshListsAndUpdateTitle();
            }
        }

        private async void ShowNewParticipantDialog()
        {
            NewParticipantDialog newParticipantDialog = new NewParticipantDialog();
            ContentDialogResult result = await newParticipantDialog.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                if (newParticipantDialog.person != null)
                {
                    if (fundRaiser.ParticipantExist(newParticipantDialog.person))
                    {
                        new MessageDialog("Ta osoba już uczestniczy w tej zbiórce.", "Błąd").ShowAsync();
                        return;
                    }

                    int index = allFundRaisers.IndexOf(fundRaiser);
                    if (index != -1)
                    {
                        if (fundRaiser.participants.Count == 0)
                        {
                            fundRaiser.AddNewParticipant(newParticipantDialog.person);
                            allFundRaisers[index] = fundRaiser;

                            if (fundRaiser.expenses != null)
                            {
                                foreach (Person person in fundRaiser.participants)
                                {
                                    person.obligation -= fundRaiser.SumUpExpences() / fundRaiser.participants.Count();
                                }
                            }
                        }
                        else
                        {
                            if (fundRaiser.expenses != null)
                            {
                                foreach (Person person in fundRaiser.participants)
                                {
                                    person.obligation += fundRaiser.SumUpExpences() / fundRaiser.participants.Count();
                                }
                            }

                            fundRaiser.AddNewParticipant(newParticipantDialog.person);
                            allFundRaisers[index] = fundRaiser;

                            if (fundRaiser.expenses != null)
                            {
                                foreach (Person person in fundRaiser.participants)
                                {
                                    person.obligation -= fundRaiser.SumUpExpences() / fundRaiser.participants.Count();
                                }
                            }
                        }
                        await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                        RefreshParticipantsListOnListView();
                        return;
                    }
                }
            }

            // Opens contacts window
            if (newParticipantDialog.pickFromContacts)
            {
                PickFromContacts();
            }
        }

        private async void PickFromContacts()
        {
            var contactPicker = new Windows.ApplicationModel.Contacts.ContactPicker();
            contactPicker.DesiredFieldsWithContactFieldType.Add(Windows.ApplicationModel.Contacts.ContactFieldType.Email);

            var contacts = await contactPicker.PickContactsAsync();

            if (contacts != null)
            {
                foreach (Contact contact in contacts)
                {
                    ContactEmail contactEmail = contact.Emails[0];
                    if (fundRaiser.ParticipantExist(new Person(contact.FirstName, contactEmail.Address)))
                    {
                        new MessageDialog("Wybrana(e) przez Ciebie osoba(y) są już na liście. Ponów próbę.", "Błąd").ShowAsync();
                        return;
                    }
                }

                if (fundRaiser.participants.Count == 0)
                {
                    int index = allFundRaisers.IndexOf(fundRaiser);
                    foreach (Contact contact in contacts)
                    {
                        ContactEmail contactEmail = contact.Emails[0];
                        if (index != -1)
                        {
                            fundRaiser.AddNewParticipant(new Person(contact.FirstName, contactEmail.Address));
                            allFundRaisers[index] = fundRaiser;
                        }
                    }

                    if (fundRaiser.expenses != null)
                    {
                        foreach (Person person in fundRaiser.participants)
                        {
                            person.obligation -= fundRaiser.SumUpExpences() / fundRaiser.participants.Count();
                        }
                    }
                }
                else
                {
                    if (fundRaiser.expenses != null)
                    {
                        foreach (Person person in fundRaiser.participants)
                        {
                            person.obligation += fundRaiser.SumUpExpences() / fundRaiser.participants.Count();
                        }
                    }

                    int index = allFundRaisers.IndexOf(fundRaiser);
                    foreach (Contact contact in contacts)
                    {
                        ContactEmail contactEmail = contact.Emails[0];
                        if (index != -1)
                        {
                            fundRaiser.AddNewParticipant(new Person(contact.FirstName, contactEmail.Address));
                            allFundRaisers[index] = fundRaiser;
                        }
                    }

                    if (fundRaiser.expenses != null)
                    {
                        foreach (Person person in fundRaiser.participants)
                        {
                            person.obligation -= fundRaiser.SumUpExpences() / fundRaiser.participants.Count();
                        }
                    }
                }

                await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                RefreshParticipantsListOnListView();
                return;
            }
        }

        private async void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RefreshListsAndUpdateTitle();
            System.Diagnostics.Debug.WriteLine(await mySerializer.ReadDataFromJsonFileToString());
        }

        private void RefreshListsAndUpdateTitle()
        {
            RefreshParticipantsListOnListView();
            RefreshExpensesListOnListView();
            if (fundRaiser.expenses == null)
            {
                UpdateTitle(passedName, 0);
                return;
            }
            UpdateTitle(passedName, fundRaiser.SumUpExpences());
        }

        private void RefreshParticipantsListOnListView()
        {
            observablePersons.Clear();
            if (fundRaiser.participants != null)
            {
                foreach (Person person in fundRaiser.participants)
                {
                    observablePersons.Add(person);
                }
                ParticipantsListView.DataContext = observablePersons;
            }
        }

        private void RefreshExpensesListOnListView()
        {
            observableExpenses.Clear();
            if (fundRaiser.expenses != null)
            {
                foreach (Expense expense in fundRaiser.expenses)
                {
                    observableExpenses.Add(expense);
                }
                ExpensesListView.DataContext = observableExpenses;
            }
        }

        private async void ParticipantsListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedPerson = e.ClickedItem as Person;

            if (clickedPerson.obligation < 0)
            {
                string title = "Wpłata";
                string messasge = String.Format("Czy {0} wpłacił(a) {1} zł?", clickedPerson.name, -1 * clickedPerson.obligation);

                string yesCommandText = "tak";
                string noCommandText = "nie";

                UICommand yesCommand = new UICommand(yesCommandText);
                UICommand noCommand = new UICommand(noCommandText);

                MessageDialog dialog = new MessageDialog(messasge, title);
                dialog.Commands.Add(yesCommand);
                dialog.Commands.Add(noCommand);

                var result = await dialog.ShowAsync();

                if (result.Label.Equals(yesCommandText))
                {
                    int index = fundRaiser.participants.IndexOf(clickedPerson);
                    clickedPerson.obligation = 0;
                    fundRaiser.participants[index] = clickedPerson;
                    await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                    RefreshParticipantsListOnListView();
                }
            }
        }

        private async void ParticipantsListView_RightTapped(object sender, RightTappedRoutedEventArgs e)
        {
            FrameworkElement element = e.OriginalSource as FrameworkElement;
            if (element.DataContext != null && element.DataContext is Person)
            {
                var holded = element.DataContext as Person;
                EditPersonDialog editPersonDialog = new EditPersonDialog(holded.obligation);
                ContentDialogResult result = await editPersonDialog.ShowAsync();

                if (result == ContentDialogResult.Primary)
                {
                    int index = fundRaiser.participants.IndexOf(holded);
                    holded.obligation = editPersonDialog.payment;
                    fundRaiser.participants[index] = holded;
                    await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                    RefreshParticipantsListOnListView();
                }

                if (editPersonDialog.isDeleteClicked)
                {
                    fundRaiser.participants.Remove(holded);

                    foreach (Person person in fundRaiser.participants)
                    {
                        person.obligation += fundRaiser.SumUpExpences() / (fundRaiser.participants.Count + 1);
                        person.obligation -= fundRaiser.SumUpExpences() / fundRaiser.participants.Count;
                    }
                    await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                    RefreshParticipantsListOnListView();
                }
            }
        }

        private void NotifyAllParticipants_Click(object sender, RoutedEventArgs e)
        {
            EmailManager.ShowComposeNewEmailAsync(new EmailBuilder(fundRaiser).BuildEmailMessage());
        }

        private async void ExpensesListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var clickedExpense = e.ClickedItem as Expense;
            EditExpenseDialog editExpenseDialog = new EditExpenseDialog(clickedExpense);
            ContentDialogResult result = await editExpenseDialog.ShowAsync();

            if (result == ContentDialogResult.Primary)
            {
                fundRaiser.expenses[fundRaiser.expenses.IndexOf(clickedExpense)] = editExpenseDialog.expense;
                foreach (Person person in fundRaiser.participants)
                {
                    person.obligation += clickedExpense.SummaryExpenseCost() / fundRaiser.participants.Count;
                    person.obligation -= editExpenseDialog.expense.SummaryExpenseCost() / fundRaiser.participants.Count;
                }
                await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                RefreshListsAndUpdateTitle();
            }

            if (editExpenseDialog.deleteButtonClicked)
            {
                fundRaiser.expenses.Remove(clickedExpense);
                foreach (Person person in fundRaiser.participants)
                {
                    person.obligation += clickedExpense.SummaryExpenseCost() / fundRaiser.participants.Count;
                }
                await mySerializer.WriteDataToJsonFileAsync(allFundRaisers);
                RefreshListsAndUpdateTitle();
            }
        }
    }

}
